/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
/*
construimos la clase searchform que sera que contenga el input y elbtn para
buscar
El placeholder es el texto por defecto que aparece en el input
*/

/*
para controlar el contenido de input generamos un state inicial al que luego pasaremos
el valor ingresado
*/

/*
definimos el Api key obtenido desde la pagina omdb
*/
const API_KEY = '1d1b1f9d'

export class SearchForm extends Component {
  //state inicial para el input
  state ={
     inputMovie:""
  }  
//creamos el evento como una arrow function que contendra el valor ingresado en input
// agregamos en el evento onchange del input la funciona_handleChange
  _handleChange = (e) => {
    this.setState({ inputMovie :e.target.value})

  }
  //envolvemos todo el div de busqueda en un formulario
  //el cual contendra el evento onSubmit que invocara a la funcion _handleSubmit
  //generamos el metodo _handleSubmit y como primer accion desabilitamos el evento inicial del formulario
 //luego emitimos un alerta con el contenido del  state inputMovie
 //definimos una constante para contener el inputmovie
 //realizamos el metodo fetch para pedir datos mediante la api de omdb
 //Siempre revisar el contrato de la api que tiene los campos requeridos y como accederlos
 //Siempre antes de pasar un valor se antepone $ y & para concatenar
 //En este caso el parametro para buscar por titulo es t
 //Cada invocacion a api es una promesa de respuesta para esto debemos consultar la misma con
 // then(res => res.json()) e indicamos que esperamos la respuesa en formato json
 // luego manejamos esta promesa de respuesta con otro then que contiene los dato
 // para pasar datos a una api usar ` en lugar de '
  _handleSubmit = (e) => {
     e.preventDefault()
     // alert(this.state.inputMovie)
          const { inputMovie } = this.state
      

        fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&s=${inputMovie}`)
    
            .then(res => res.json())
            .then(results => {
              const {Search =[], totalResults = "0"} = results
        
              this.props.onResults(Search)
              //hay que parser los parametros devueltos por la api ver que distinge mayuscula y minuncula
          
              
        })
  }


  //la pagina por defecto toma el ultimo btn para enviar el formulario siempre y cuando
  //halla definido otro, en este caso al dar click en buscar envia el formulario
  render() {
        return (
          <form className="miForm" onSubmit={this._handleSubmit}>
            <div className="field has-addons">
              <div className="control">
                <input
                className="input"
                onChange={this._handleChange} 
                placeholder="Pelicula a Buscar..."
                type="text"/>
              </div>
              <div className="control">
                <button className="button is-info">
                  Buscar
                </button>
              </div>
            </div>
            </form>
        );
    }
}