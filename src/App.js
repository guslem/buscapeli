import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import {Detail} from  './pages/Detail';
import {NotFound} from './pages/NotFound';
import './App.css';
import 'bulma/css/bulma.css';
import { Home } from './pages/Home';




  //definimos el state para recibir el contenido de la api
/*
0:
Poster: "https://m.media-amazon.com/images/M/MV5BMjE5NDQ5OTE4Ml5BMl5BanBnXkFtZTcwOTE3NDIzMw@@._V1_SX300.jpg"
Title: "Star Trek"
Type: "movie"
Year: "2009"
imdbID: "tt0796366"
*/

class App extends Component {

render() {
/* creamos una url usando el document locatio
luego usamos el searchParam.has de la url para identificar si tiene el parametro id
si lo tiene ruteamos el render en Detail sino lo dejamos como esta
*/  

/*
const url= new URL(document.location)
const hasId= url.searchParams.has("id")
//const idMovie=url.searchParams.get('id')
/*render condicional
recuperamos el id de la pagina a la que se la estamos pasando para luego usarlo en un fetch
a la api
*/
//? <Detail id={(url.searchParams.get('id'))}/>
//: <Home/>


/*
if (hasId) {

console.log("has id ", idMovie )

  return <Detail id={idMovie}/>
}*/

  return (
    <div className="App">
  <Switch>
      <Route exact path='/' component={Home} />
      <Route path='/detail/:id' component={Detail}/>
      <Route component={NotFound}/>
      </Switch>
    </div>
  );
}
}

export default App;
