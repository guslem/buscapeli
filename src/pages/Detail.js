import React, {Component} from 'react';
import PropTypes  from 'prop-types';
import { BotonHome } from '../components/BotonHome';


const API_KEY = '1d1b1f9d'

export class Detail extends Component {
   static propTypes = {
  //    id : PropTypes.string
       match: PropTypes.shape({
          params:PropTypes.object,
          isExact: PropTypes.bool,
          path: PropTypes.string,
          url:PropTypes.string
       })  

}

  /*en el ciclo de vida indica que ya se ha ejecutado una vez
  */
 
   state = { movie: {} }

   _fetchMovie ({id}) {
      
  console.log("entre a fetchmovie", id )


  fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&i=${id}`)
          
   .then(res => res.json())
   .then(movie => {
    // const {Search =[], totalResults = "0"} = results
    
     this.setState({movie})
   //  this.props.onResults(Search)
     //hay que parser los parametros devueltos por la api ver que distinge mayuscula y minuncula
 
     
})
  }
  //el metodo window.history vuelve a la pagina anterior
/*
  _goBack() {
 window.history.back()

}
<button onClick={this._goBack}>Volver</button>
*/  
componentDidMount (){
   const { id} = this.props.match.params
   this._fetchMovie({id})
}
 render () {
    const { Title, Actors, Director, Genre, imdbRating, Poster} = this.state.movie
    return (
       <div className="MovieList-item">
       
         <BotonHome/>
         <h1>{Title}</h1>
          <img src={Poster} alt={Title}/> 
         <h3>{Actors}</h3>
         <span>{imdbRating}</span>
         <p>{Genre}</p>
         <p>{Director}</p>
         </div>
     
 )
    }
}


