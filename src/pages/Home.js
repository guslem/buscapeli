import React, {Component} from 'react';
import { Title, Subtitle } from '../components/Title';
import {  SearchForm }  from '../components/SearchForm';
import {MoviesList} from '../components/MoviesList';

export class Home extends Component {
  //Generamos el metodo handleResult para pasar el contenido de la clase searchForm 
  //y recibirlos en este objeto results que es un array
  state = { userSearch:false,  results: [] }

  _renderResult() {
    return ( this.state.results.length === 0
      ? <p>Lo lamento <span>:(</span> no hubo Resultados</p>
      : <MoviesList movies={this.state.results}/>
  )
     
  }

//esto se pasa como metodo a la clase searchform
  _handleResults = (results) => {
    this.setState({ results , userSearch:true})
  }
/*  //reemplazamos el metodo _handleResult por una clase reutilizable
_renderResult =(results) => {
    const  {results } = this.state
    return results.map(movie => {
    
      return  <Movie 
                key={movie.imdbID}
                title={movie.Title}
                year={movie.Year}
                poster={movie.Poster}
                />
    })
*/
    render() {
        return (
            <div>
                <Title>Buscador de peliculas</Title>
                <Subtitle>Mi primera App con react</Subtitle>
                <small>Martin Lemos 2020</small>
                <div className="searchForm-wrapper">
                <SearchForm onResults={this._handleResults} />
                
                </div>
                { this.state.userSearch
                ? this._renderResult()
                : <small>Use el buscador para obtener resultados :)</small>
                }
      
            </div>
        )
    }
}