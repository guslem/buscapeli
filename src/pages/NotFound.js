import React from 'react';
import { Title, Subtitle } from '../components/Title';
import { BotonHome } from '../components/BotonHome';

export const NotFound = () => (
  
   <div>
       <Title>404!</Title>
       <Subtitle>Pagina no encontrada</Subtitle>
       <BotonHome/>
   </div>



)